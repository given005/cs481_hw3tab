﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Square
{
    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();

            MainPage = new Square.TabbedPage1();
        }

        protected override void OnStart()
        {
        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }
    }
}
